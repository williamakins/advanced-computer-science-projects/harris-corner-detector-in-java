import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Source extends Component implements KeyListener {
    private BufferedImage in, out;
    int width, height;
    File inputFile;

    public Source() {
        loadImage();
        addKeyListener(this);
    }

    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }

    public void paint(Graphics g) {
        g.drawImage(out, 0, 0, null);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("William Akins - Harris Corner Detector");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Source img = new Source();
        frame.add("Center", img);
        frame.pack();
        img.requestFocusInWindow();
        frame.setVisible(true);
    }

    private void processing() {
        int windowSize = 5;

        int xOffset = (int)(windowSize * 0.5f);
        int yOffset = (int)(windowSize * 0.5f);

        int xWindows = (width / windowSize);
        int yWindows = (height / windowSize);

        float edgeThreshold = 17.0f;

        System.out.println("Number of Windows: X= " + xWindows + ", Y= " + yWindows);

        //This is the first pass of the image, where the X and Y offset are set to 0
        float[][] windowIntensity = calculateIntensity(windowSize, xWindows, yWindows, 0, 0);

        //This is the second pass of the image with each window being offset in the X and Y axis, it is used to then
        //calculate the difference or displacement of the first and second pass
        float[][] offsetWindowIntensity = calculateIntensity(windowSize, xWindows, yWindows, xOffset, yOffset);

        //loop through all the windows, calculate the displacement intensity of each window
        //this is the difference between the first pass of the image and the second when the windows was offset in the X and Y axis
        float[][] displacementWindowIntensity = new float[xWindows][yWindows];

        for (int x = 1; x < xWindows - 1; x++) {
            for (int y = 1; y < yWindows - 1; y++) {

                //Sobel Operator X kernel
                int[][] Gx = {{-1, 0, 1},
                        {-2, 0, 2},
                        {-1, 0, 1}};

                //Sobel Operator Y kernel
                int[][] Gy = {{-1, -2, -1},
                        {0, 0, 0},
                        {1, 2, 1}};

                //make use of the sobel operator on the windows, this will enhance the visibility of edges for use with
                //the detection procedure
                float tmpWindowX = windowIntensity[x + 1][y - 1] * Gx[0][2];
                tmpWindowX += windowIntensity[x + 1][y] * Gx[1][2];
                tmpWindowX += windowIntensity[x + 1][y + 1] * Gx[2][2];
                tmpWindowX += windowIntensity[x - 1][y - 1] * Gx[0][0];
                tmpWindowX += windowIntensity[x - 1][y] * Gx[0][1];
                tmpWindowX += windowIntensity[x - 1][y + 1] * Gx[0][2];
                tmpWindowX = Math.abs(tmpWindowX);

                float tmpWindowY = windowIntensity[x + 1][y - 1] * Gy[0][2];
                tmpWindowY += windowIntensity[x + 1][y] * Gy[1][2];
                tmpWindowY += windowIntensity[x + 1][y + 1] * Gy[2][2];
                tmpWindowY += windowIntensity[x - 1][y - 1] * Gy[0][0];
                tmpWindowY += windowIntensity[x - 1][y] * Gy[0][1];
                tmpWindowY += windowIntensity[x - 1][y + 1] * Gy[0][2];
                tmpWindowY = Math.abs(tmpWindowY);

                float tmpOffsetX = offsetWindowIntensity[x + 1][y - 1] * Gx[0][2];
                tmpOffsetX += offsetWindowIntensity[x + 1][y] * Gx[1][2];
                tmpOffsetX += offsetWindowIntensity[x + 1][y + 1] * Gx[2][2];
                tmpOffsetX += offsetWindowIntensity[x - 1][y - 1] * Gx[0][0];
                tmpOffsetX += offsetWindowIntensity[x - 1][y] * Gx[0][1];
                tmpOffsetX += offsetWindowIntensity[x - 1][y + 1] * Gx[0][2];
                tmpOffsetX = Math.abs(tmpOffsetX);

                float tmpOffsetY = offsetWindowIntensity[x + 1][y - 1] * Gy[0][2];
                tmpOffsetY += offsetWindowIntensity[x + 1][y] * Gy[1][2];
                tmpOffsetY += offsetWindowIntensity[x + 1][y + 1] * Gy[2][2];
                tmpOffsetY += offsetWindowIntensity[x - 1][y - 1] * Gy[0][0];
                tmpOffsetY += offsetWindowIntensity[x - 1][y] * Gy[0][1];
                tmpOffsetY += offsetWindowIntensity[x - 1][y + 1] * Gy[0][2];
                tmpOffsetY = Math.abs(tmpOffsetY);

                //System.out.println("vals: X= " + tmpWindowX + ", Y= " + tmpWindowY);

                //based off the output of the Sobel operator function, calculate the magnitude for the window and the offset window
                float windowMagnitude = (float)Math.sqrt((tmpWindowX * tmpWindowX) + (tmpWindowY * tmpWindowY));
                float offsetMagnitude = (float)Math.sqrt((tmpOffsetX * tmpOffsetX) + (tmpOffsetY * tmpOffsetY));

                displacementWindowIntensity[x][y] = Math.abs(Math.abs(offsetMagnitude) - (Math.abs(windowMagnitude)));

                //displacementWindowIntensity[x][y] = Math.abs(offsetWindowIntensity[x][y] - windowIntensity[x][y]);
            }
        }

        //loop through the entire image to display the outcome of the edge detector visually
        for (int u = 0; u < xWindows; u++) { //image X
            for (int v = 0; v < yWindows; v++) { //image Y
                //determine if the evaluated window meets the minimum threshold to be classed as an edge
                if (displacementWindowIntensity[u][v] > edgeThreshold) {
                    //loop through the window size
                    for (int x = u * windowSize; x < (u * windowSize) + windowSize; x++) { //Window X
                        for (int y = v * windowSize; y < (v * windowSize) + windowSize; y++) { //Window Y
                            //draw a hollow box to represent the location of a window which met the intensity threshold for an edge
                            if (x == u * windowSize || x == ((u * windowSize) + windowSize) - 1 || y == v * windowSize || y == ((v * windowSize) + windowSize) - 1) {
                                out.setRGB(x, y, (new Color(255, 255, 255)).getRGB());
                            }
                        }
                    }
                }
            }
        }

        repaint();
    }

    private float[][] calculateIntensity(int windowSize, int xWindows, int yWindows, int xOffset, int yOffset) {
        //local temp store for the window intensity
        float[][] intensity = new float[xWindows][yWindows];

        //loop through the entire picture
        for (int u = 0; u < xWindows; u++) { //image X
            int uw = u * windowSize;
            for (int v = 0; v < yWindows; v++) { //image Y
                //loop through the window, drawing a box on screen to represent where the edge is
                for (int x = uw + xOffset; x < windowSize + uw + xOffset; x++) { //Window X
                    int vw = v * windowSize;
                    for (int y = vw + yOffset; y < windowSize + vw + yOffset; y++) { //Window Y
                        //prevent the code trying to access a pixel outside of the image boundaries
                        if (x >= 0 && y >= 0 && x < width && y < height) {
                            //get the colour of the current pixel being evaluated
                            Color pixel = new Color(in.getRGB(x, y));
                            //store the intensity of that pixel, in order to calculate the total intensity of the window
                            intensity[u][v] += (pixel.getRed() + pixel.getGreen() + pixel.getBlue()) / 255.0f;
                        }
                    }
                }
            }
        }

        return intensity;
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_ESCAPE)
            System.exit(0);

        //Press S to save the image to the root folder for the program
        if (ke.getKeyChar() == 's' || ke.getKeyChar() == 'S') {
            saveImage();
            //press P to begin the processing for the Harris Corner Detector
        } else if (ke.getKeyChar() == 'p' || ke.getKeyChar() == 'P') {
            processing();
        }
    }

    private void loadImage() {
        //attempt to load an image from the root folder for the program, should it not detect the image then exit the program
        try {
            in = ImageIO.read(new File("apple.jpg"));
            width = in.getWidth();
            height = in.getHeight();
            out = in;
        } catch (IOException e) {
            System.out.println("Image could not be read");
            System.exit(1);
        }
    }

    private void saveImage() {
        try {
            ImageIO.write(out, "jpg", new File("WelshDragonP.jpg"));
        } catch (IOException ex) {
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub

    }
}